#include "stdafx.h"
#include <stdio.h>
#include <assert.h>
#include <windows.h> 
#include <lm.h>
//#include <sddl.h>


#ifndef UNICODE
#define UNICODE
#endif
#pragma comment(lib, "netapi32.lib")


// Type for User Account Flags
typedef struct {
	DWORD bits;
	const TCHAR *name;
} flag_t;



// Account Flag Array
static flag_t acctFlags[] =
{
	{ UF_SCRIPT, _T("UF_SCRIPT") },
	{ UF_ACCOUNTDISABLE, _T("UF_ACCOUNTDISABLE") },
	{ UF_HOMEDIR_REQUIRED, _T("UF_HOMEDIR_REQUIRED") },
	{ UF_PASSWD_NOTREQD, _T("UF_PASSWD_NOTREQD") },
	{ UF_PASSWD_CANT_CHANGE, _T("UF_PASSWD_CANT_CHANGE") },
	{ UF_PASSWORD_EXPIRED, _T("UF_PASSWORD_EXPIRED") },
	{ UF_LOCKOUT, _T("UF_LOCKOUT") },
	{ UF_DONT_EXPIRE_PASSWD, _T("UF_DONT_EXPIRE_PASSWD") },
#ifdef UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED
	{ UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED, _T("UF_ENCRYPTED_TEXT_PASSWORD_ALLOWED") },
#endif
#ifdef UF_NOT_DELEGATED
	{ UF_NOT_DELEGATED, _T("UF_NOT_DELEGATED") },
#endif
#ifdef UF_SMARTCARD_REQUIRED
	{ UF_SMARTCARD_REQUIRED, _T("UF_SMARTCARD_REQUIRED") },
#endif
	{ UF_USE_DES_KEY_ONLY, _T("UF_USE_DES_KEY_ONLY") },
	{ UF_DONT_REQUIRE_PREAUTH, _T("UF_DONT_REQUIRE_PREAUTH") },
	{ UF_TRUSTED_FOR_DELEGATION, _T("UF_TRUSTED_FOR_DELEGATION") },
	{ UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION, _T("UF_TRUSTED_TO_AUTHENTICATE_FOR_DELEGATION") },
	{ UF_NORMAL_ACCOUNT, _T("UF_NORMAL_ACCOUNT") },
	{ UF_TEMP_DUPLICATE_ACCOUNT, _T("UF_TEMP_DUPLICATE_ACCOUNT") },
	{ UF_WORKSTATION_TRUST_ACCOUNT, _T("UF_WORKSTATION_TRUST_ACCOUNT") },
	{ UF_SERVER_TRUST_ACCOUNT, _T("UF_SERVER_TRUST_ACCOUNT") },
	{ UF_INTERDOMAIN_TRUST_ACCOUNT, _T("UF_INTERDOMAIN_TRUST_ACCOUNT") },
	{ 0, 0 }
};

const TCHAR *buildFlags(DWORD bits, flag_t *defs)
{
	static TCHAR s[4096];
	TCHAR *p = &s[0];

	for (; defs->name != 0; ++defs)
	{
		if ((defs->bits & bits) == defs->bits)
		{
			if (p != &s[0])
			{
				_tcscpy_s(p, 4096, _T(", "));
				p += _tcslen(p);
			}
			_tcscpy_s(p, 4096, defs->name);
			p += _tcslen(p);
		}
	}

	return &s[0];
}

/*!
 * @brief     Enumerate users and their basic inforation on a specified host.
 * @param[in] pszServerName pointer to a constant string that specifies the DNS or 
 *	          NetBIOS name of the remote server on which the function is to execute. 
 *	          If this parameter is NULL, the local computer is used.
 * @return    value of 0 for success or value of 1 for failure.
 */
int EnumUserAccounts(LPTSTR pszServerName)
{
	// User enumeration value enumeration
	LPUSER_INFO_23 pBuf = NULL;
	LPUSER_INFO_23 pTmpBuf;
	DWORD dwLevel = 20;
	DWORD dwPrefMaxLen = MAX_PREFERRED_LENGTH;
	DWORD dwEntriesRead = 0;
	DWORD dwTotalEntries = 0;
	DWORD dwResumeHandle = 0;
	DWORD i;
	DWORD dwTotalCount = 0;


	NET_API_STATUS nStatus;

	wprintf(L"\n[*] User account on %s: \n", pszServerName);
	do // begin do
	{
		nStatus = NetUserEnum((LPCWSTR)pszServerName,
			dwLevel,
			0, // global users
			(LPBYTE*)&pBuf,
			dwPrefMaxLen,
			&dwEntriesRead,
			&dwTotalEntries,
			&dwResumeHandle);

		if ((nStatus == NERR_Success) || (nStatus == ERROR_MORE_DATA))
		{
			if ((pTmpBuf = pBuf) != NULL)
			{
				for (i = 0; (i < dwEntriesRead); i++)
				{
					assert(pTmpBuf != NULL);

					if (pTmpBuf == NULL)
					{
						fprintf(stderr, "[-] An access violation has occurred\n");
						break;
					}
					wprintf(L"[*]\tUser Name: %s\n", pTmpBuf->usri23_name);
					wprintf(L"[*]\tFull Name: %s\n", pTmpBuf->usri23_full_name);
					wprintf(L"[*]\tComment: %s\n", pTmpBuf->usri23_comment);
					wprintf(L"[*]\tAuth Flags: %s\n", buildFlags(pTmpBuf->usri23_flags, acctFlags));
					wprintf(L"[*]\n");
					pTmpBuf++;
					dwTotalCount++;
				}
			}
		}
		else
		{
			fprintf(stderr, "A system error has occurred: %d\n", nStatus);
		}

		if (pBuf != NULL)
		{
			NetApiBufferFree(pBuf);
			pBuf = NULL;
		}
	}

	while (nStatus == ERROR_MORE_DATA); // end do

	if (pBuf != NULL)
	{
		NetApiBufferFree(pBuf);
	}
	fprintf(stderr, "[*]\n[*] Total of %d entries enumerated\n", dwTotalCount);

	return 0;
}

/*!
* @brief     Creates a local account on the specified server.
* @param[in] pszServerName pointer to a constant string that specifies the DNS or
*	          NetBIOS name of the remote server on which the function is to execute.
*	          If this parameter is NULL, the local computer is used.
* @param[in] UserName the name of the account to create on the specified host.
* @param[in] Password the password of the account.
* @return    value of 0 for success or value of 1 for failure.
*/
int AddAccount(LPTSTR pszServerName, LPTSTR UserName, LPTSTR Password)
{
	USER_INFO_1 usr_info;
	DWORD dwLevel = 1;
	DWORD dwError = 0;
	int returnval = 0;
	NET_API_STATUS nStatus;

	usr_info.usri1_name = UserName;
	usr_info.usri1_password = Password; 
	usr_info.usri1_priv = USER_PRIV_USER;
	usr_info.usri1_comment = L" ";
	usr_info.usri1_flags = (UF_SCRIPT | UF_DONT_EXPIRE_PASSWD | UF_PASSWD_CANT_CHANGE);
	usr_info.usri1_script_path = NULL;
	usr_info.usri1_home_dir = NULL;
	
	wprintf(L"\n\n[*] Creating %s user on %s machine.\n", UserName, pszServerName);

	nStatus = NetUserAdd(pszServerName, dwLevel, (LPBYTE)&usr_info, &dwError);
	wprintf(L"after creation");

	switch (nStatus)
	{
		case NERR_Success:
			wprintf(L"[*]\t%s user has been successfully added on %s machine.\n", UserName, pszServerName);
			wprintf(L"[*]\tUsername: %s password: %s.\n", UserName, Password);
			break;
		case ERROR_ACCESS_DENIED:
			wprintf(L"[-] Access Denied\n");
			returnval = 1;
			break;
		case NERR_InvalidComputer:
			wprintf(L"[-] Computer name is invalid.\n");
			returnval = 1;
			break;
		case NERR_UserExists:
			wprintf(L"[-] User already exists.\n");
			returnval = 1;
			break;
		case NERR_PasswordTooShort:
			wprintf(L"[-] Password does not meet policy.\n");
			returnval = 1;
			break;
		default:
			wprintf(L"[-] Unknown Error %d\n", nStatus);
			returnval = 1;
			break;
	}	
	return returnval;
}

/*!
* @brief     Removes a specified account on the server.
* @param[in] pszServerName pointer to a constant string that specifies the DNS or
*	          NetBIOS name of the remote server on which the function is to execute.
*	          If this parameter is NULL, the local computer is used.
* @param[in] UserName the name of the account to remove.
* @return    value of 0 for success or value of 1 for failure.
*/
int RemoveAccount(LPTSTR pszServerName, LPTSTR UserName)
{
	DWORD dwError = 0;
	int returnval = 0;
	NET_API_STATUS nStatus;
	
	wprintf(L"[*]\tDeleting user %s on %s\n", UserName, pszServerName);

	nStatus = NetUserDel(pszServerName, UserName);
	
	switch (nStatus)
	{
	case NERR_Success:
		wprintf(L"[*]\tUser %s has been successfully deleted on %s\n",
			UserName, pszServerName);
		break;
	case ERROR_ACCESS_DENIED:
		wprintf(L"[-] Access Denied\n");
		returnval = 1;
		break;
	case NERR_InvalidComputer:
		wprintf(L"[-] Computer name is invalid.\n");
		returnval = 1;
		break;
	case NERR_UserNotFound:
		wprintf(L"[-] User not found.\n");
		returnval = 1;
		break;
	}

	return returnval;
}

/*!
* @brief     Enumerate the Global Groups an account is a member of.
* @param[in] pszServerName pointer to a constant string that specifies the DNS or
*	          NetBIOS name of the remote server on which the function is to execute.
*	          If this parameter is NULL, the local computer is used.
* @param[in] UserName the name of the account to enumerate Global Group membership.
* @return    value of 0 for success or value of 1 for failure.
*/
int GetUserGlobalGroups(LPTSTR pszServerName, LPTSTR UserName)
{
	LPGROUP_USERS_INFO_0 pBuf = NULL;
	DWORD dwLevel = 0;
	DWORD dwPrefMaxLen = MAX_PREFERRED_LENGTH;
	DWORD dwEntriesRead = 0;
	DWORD dwTotalEntries = 0;
	NET_API_STATUS nStatus;
	INT returnval = 0;

	wprintf(L"[*]\tEnumerating Global Group membership for user %s on %s\n", UserName, pszServerName);

	nStatus = NetUserGetGroups(pszServerName,
	     						UserName,
								dwLevel,
								(LPBYTE*)&pBuf,
								dwPrefMaxLen,
								&dwEntriesRead,
								&dwTotalEntries);

	if (nStatus == NERR_Success)
	{
		LPGROUP_USERS_INFO_0 pTmpBuf;
		DWORD i;
		DWORD dwTotalCount = 0;

		if ((pTmpBuf = pBuf) != NULL)
		{
			fprintf(stderr, "\n[*]\tGlobal group(s):\n");
			for (i = 0; i < dwEntriesRead; i++)
			{
				assert(pTmpBuf != NULL);

				if (pTmpBuf == NULL)
				{
					fprintf(stderr, "[-] An access violation has occurred\n");
					break;
				}

				wprintf(L"[*]\t%s\n", pTmpBuf->grui0_name);

				pTmpBuf++;
				dwTotalCount++;
			}
		}
		
		if (dwEntriesRead < dwTotalEntries)
		{
			fprintf(stderr, "\n[*]\tTotal entries: %d", dwTotalEntries);
		}
		printf("\n[*]\tEntries enumerated: %d\n", dwTotalCount);
	}
	else
	{
		switch (nStatus)
		{
		case NERR_Success:
			wprintf(L"[*]\tUser %s has been successfully deleted on %s\n",
				UserName, pszServerName);
			break;
		case ERROR_ACCESS_DENIED:
			wprintf(L"[-] Access Denied\n");
			returnval = 1;
			break;
		case NERR_DCNotFound:
			wprintf(L"[-] Domain Controller not found.\n");
			returnval = 1;
			break;
		case NERR_UserNotFound:
			wprintf(L"[-] User not found.\n");
			returnval = 1;
			break;
		case RPC_S_SERVER_UNAVAILABLE:
			wprintf(L"[-] RPC Service unavailable.\n");
			returnval = 1;
			break;
		case ERROR_NOT_ENOUGH_MEMORY:
			wprintf(L"[-] Insufficient memory was available to complete the operation.\n");
			returnval = 1;
			break;
		case ERROR_MORE_DATA:
			wprintf(L"[-] More entries are available than what buffer could handle.\n");
			returnval = 1;
			break;
		default:
			wprintf(L"[-] Unknown Error %d\n", nStatus);
			returnval = 1;
			break;
		}
	}

	if (pBuf != NULL)
	{
		NetApiBufferFree(pBuf);
	}

	return returnval;
}

/*!
* @brief     Enumerate the Local Groups an account is a member of.
* @param[in] pszServerName pointer to a constant string that specifies the DNS or
*	          NetBIOS name of the remote server on which the function is to execute.
*	          If this parameter is NULL, the local computer is used.
* @param[in] UserName the name of the account to enumerate Local Group membership.
* @return    value of 0 for success or value of 1 for failure.
*/
int GetUserLocalGroups(LPTSTR pszServerName, LPTSTR UserName)
{
	LPLOCALGROUP_USERS_INFO_0 pBuf = NULL;
	DWORD dwLevel = 0;
	DWORD dwFlags = LG_INCLUDE_INDIRECT;
	DWORD dwPrefMaxLen = MAX_PREFERRED_LENGTH;
	DWORD dwEntriesRead = 0;
	DWORD dwTotalEntries = 0;
	NET_API_STATUS nStatus;
	INT returnval = 0;

	wprintf(L"[*]\tEnumerating Local Group membership for user %s on %s\n", UserName, pszServerName);

	nStatus = NetUserGetLocalGroups(pszServerName,
									UserName,
									dwLevel,
									dwFlags,
									(LPBYTE *)&pBuf,
									dwPrefMaxLen,
									&dwEntriesRead,
									&dwTotalEntries);

	if (nStatus == NERR_Success)
	{
		LPLOCALGROUP_USERS_INFO_0 pTmpBuf;
		DWORD i;
		DWORD dwTotalCount = 0;

		if ((pTmpBuf = pBuf) != NULL)
		{
			fprintf(stderr, "[*]\tLocal group(s):\n");
			for (i = 0; i < dwEntriesRead; i++)
			{
				assert(pTmpBuf != NULL);

				if (pTmpBuf == NULL)
				{
					fprintf(stderr, "[-] An access violation has occurred\n");
					break;
				}

				wprintf(L"[*]\t\t%s\n", pTmpBuf->lgrui0_name);

				pTmpBuf++;
				dwTotalCount++;
			}
		}

		if (dwEntriesRead < dwTotalEntries)
		{
			fprintf(stderr, "[*]\tTotal entries: %d", dwTotalEntries);
		}
		printf("[*]\tEntries enumerated: %d\n", dwTotalCount);
	}
	else
	{
		switch (nStatus)
		{
		case NERR_Success:
			wprintf(L"[*]\tUser %s has been successfully deleted on %s\n",
				UserName, pszServerName);
			break;
		case ERROR_ACCESS_DENIED:
			wprintf(L"[-] Access Denied\n");
			returnval = 1;
			break;
		case NERR_DCNotFound:
			wprintf(L"[-] Domain Controller not found.\n");
			returnval = 1;
			break;
		case NERR_UserNotFound:
			wprintf(L"[-] User not found.\n");
			returnval = 1;
			break;
		case RPC_S_SERVER_UNAVAILABLE:
			wprintf(L"[-] RPC Service unavailable.\n");
			returnval = 1;
			break;
		case ERROR_NOT_ENOUGH_MEMORY:
			wprintf(L"[-] Insufficient memory was available to complete the operation.\n");
			returnval = 1;
			break;
		case ERROR_MORE_DATA:
			wprintf(L"[-] More entries are available than what buffer could handle.\n");
			returnval = 1;
			break;
		default:
			wprintf(L"[-] Unknown Error %d\n", nStatus);
			returnval = 1;
			break;
		}
	}

	if (pBuf != NULL)
	{
		NetApiBufferFree(pBuf);
	}

	return returnval;
}

/*!
* @brief     Enumerate the Global Groups an account is a member of.
* @param[in] pszServerName pointer to a constant string that specifies the DNS or
*	          NetBIOS name of the remote server on which the function is to execute.
*	          If this parameter is NULL, the local computer is used.
* @param[in] UserName the name of the account to reset the password.
* @param[in] Password the new password of the account.
* @return    value of 0 for success or value of 1 for failure.
*/
int ResetUserPassword(LPTSTR pszServerName, LPTSTR UserName, LPTSTR Password)
{
	DWORD dwLevel = 1003;
	USER_INFO_1003 ui;
	int returnval = 0;
	NET_API_STATUS nStatus;

	ui.usri1003_password = Password;

	nStatus = NetUserSetInfo(pszServerName,
		UserName,
		dwLevel,
		(LPBYTE)&ui,
		NULL);

	switch (nStatus)
	{
	case NERR_Success:
		wprintf(L"[*]\t%s user has been successfully added on %s machine.\n", UserName, pszServerName);
		wprintf(L"[*]\tUsername: %s password: %s.\n", UserName, Password);
		break;
	case ERROR_ACCESS_DENIED:
		wprintf(L"[-] Access Denied\n");
		returnval = 1;
		break;
	case NERR_InvalidComputer:
		wprintf(L"[-] Computer name is invalid.\n");
		returnval = 1;
		break;
	case NERR_UserNotFound:
		wprintf(L"[-] User not found.\n");
		returnval = 1;
		break;
	case NERR_PasswordTooShort:
		wprintf(L"[-] Password does not meet policy.\n");
		returnval = 1;
		break;
	default:
		wprintf(L"[-] Unknown Error %d\n", nStatus);
		returnval = 1;
		break;
	}

	return returnval;
}

/*!
* @brief     Identifies the main DC for the Site a host is a member of or for a
*            specified domain.
* @param[in] pszServerName pointer to a constant string that specifies the DNS or
*	          NetBIOS name of the remote server on which the function is to execute.
*	          If this parameter is NULL, the local computer is used.
* @param[in] DomainName A pointer to a constant string that specifies 
*		     the name of the domain. The domain name must be a NetBIOS domain name.
*            If NULL is specified the domain the host is a member of will be used.
* @return    value of 0 for success or value of 1 for failure.
*/
int GetDCName(LPTSTR pszServerName, LPTSTR DomainName)
{
	NET_API_STATUS nStatus;
	LPCWSTR lpDcName = NULL;
	int returnval = 0;
	
	nStatus = NetGetDCName(pszServerName, DomainName, (LPBYTE *)&lpDcName);

	if (nStatus == NERR_Success) 
	{
		wprintf(L"[*]\tDC Name %ws\n", lpDcName);
		// Need to free the returned buffer
		NetApiBufferFree((LPVOID)lpDcName);
	}
	else
	{
		switch (nStatus) 
		{
		case ERROR_INVALID_PARAMETER:
			wprintf(L"ERROR_INVALID_PARAMETER\n");
			returnval = 1;
			break;
		case ERROR_NO_SUCH_DOMAIN:
			wprintf(L"ERROR_NO_SUCH_DOMAIN\n");
			returnval = 1;
			break;
		case ERROR_NOT_SUPPORTED:
			wprintf(L"ERROR_NOT_SUPPORTED\n");
			returnval = 1;
			break;
		case ERROR_BAD_NETPATH:
			wprintf(L"ERROR_BAD_NETPATH\n");
			returnval = 1;
			break;
		case ERROR_INVALID_COMPUTERNAME:
			wprintf(L"ERROR_INVALID_COMPUTERNAME\n");
			returnval = 1;
			break;
		case DNS_ERROR_INVALID_NAME_CHAR:
			wprintf(L"DNS_ERROR_INVALID_NAME_CHAR\n");
			returnval = 1;
			break;
		case DNS_ERROR_NON_RFC_NAME:
			wprintf(L"DNS_ERROR_NON_RFC_NAME\n");
			returnval = 1;
			break;
		case ERROR_INVALID_NAME:
			wprintf(L"ERROR_INVALID_NAME\n");
			returnval = 1;
			break;
		case NERR_DCNotFound:
			wprintf(L"NERR_DCNotFound\n");
			returnval = 1;
			break;
		case NERR_WkstaNotStarted:
			wprintf(L"NERR_WkstaNotStarted\n");
			returnval = 1;
			break;
		case RPC_S_SERVER_UNAVAILABLE:
			wprintf(L"RPC_S_SERVER_UNAVAILABLE\n");
			returnval = 1;
			break;
		case RPC_E_REMOTE_DISABLED:
			wprintf(L"RPC_E_REMOTE_DISABLED\n");
			returnval = 1;
			break;
		default:
			wprintf(L"[-] Unknown Error %d\n", nStatus);
			break;
		}
	}

	return returnval;
}

