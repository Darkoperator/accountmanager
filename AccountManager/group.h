#include <windows.h> 
#include <lm.h>
#include <stdio.h>

int AddGroupMember(LPTSTR pszServerName, LPTSTR UserName, LPTSTR GroupName);
int AddLocalGroup(LPTSTR pszServerName, LPTSTR GroupName, LPTSTR Comment);
int RemoveLocalGroup(LPTSTR pszServerName, LPTSTR GroupName);