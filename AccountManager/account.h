//#include "stdafx.h"
#include <windows.h> 
#include <lm.h>
#ifndef UNICODE
#define UNICODE
#endif
typedef struct {
	LPTSTR name;
	LPTSTR comment;
	DWORD version_major;
	DWORD version_minor;
} serverinfo_t;

int EnumUserAccounts( LPTSTR pszServerName );
int AddAccount(LPTSTR pszServerName, LPTSTR UserName, LPTSTR Password);
int RemoveAccount(LPTSTR pszServerName, LPTSTR UserName);
int GetUserGlobalGroups(LPTSTR pszServerName, LPTSTR UserName);
int GetUserLocalGroups(LPTSTR pszServerName, LPTSTR UserName);
int ResetUserPassword(LPTSTR pszServerName, LPTSTR UserName, LPTSTR Password);
int GetDCName(LPTSTR pszServerName, LPTSTR DomainName);

