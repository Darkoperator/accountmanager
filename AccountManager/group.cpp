#include <windows.h> 
#include <lm.h>
#include <stdio.h>

/*!
* @brief     Add a User to a either a local or global group.
* @param[in] pszServerName pointer to a constant string that specifies the DNS or
*	          NetBIOS name of the remote server on which the function is to execute.
*	          If this parameter is NULL, the local computer is used.
* @param[in] UserName the name of the account to enumerate Local Group membership.
* @param[in] GroupName Pointer to a constant string that specifies the name of the
* 			 local group to which the specified users or global groups will be added.
* @return    value of 0 for success or value of 1 for failure.
*/
int AddGroupMember(LPTSTR pszServerName, LPTSTR UserName, LPTSTR GroupName)
{
	// Parameters for local Users Group
	LOCALGROUP_MEMBERS_INFO_3 lmi3;
	ZeroMemory(&lmi3, sizeof lmi3);
	lmi3.lgrmi3_domainandname = UserName;
	NET_API_STATUS nStatus;
	int returnval = 0;

	nStatus = NetLocalGroupAddMembers(pszServerName, GroupName, 3, (LPBYTE)&lmi3, 1);
	switch (nStatus)
	{
	case NERR_Success:
		wprintf(L"[*]\t%s user has been successfully on %s machine to the group %s.\n", UserName, pszServerName, GroupName);
		break;
	case ERROR_ACCESS_DENIED:
		wprintf(L"[-] Access Denied\n");
		returnval = 1;
		break;
	case NERR_GroupNotFound:
		wprintf(L"[-] Group specified does not exist.\n");
		returnval = 1;
		break;
	case ERROR_MEMBER_IN_ALIAS:
		wprintf(L"[-] The member specified were already members of the group.\n");
		returnval = 0;
		break;
	case ERROR_INVALID_MEMBER:
		wprintf(L"[-] The member cannot be added because the account type is invalid.\n");
		returnval = 1;
		break;
	default:
		wprintf(L"[-] Unknown Error %d\n", nStatus);
		returnval = 1;
		break;
	}

	return returnval;
}

int AddLocalGroup(LPTSTR pszServerName, LPTSTR GroupName, LPTSTR Comment)
{
	LOCALGROUP_INFO_1 lgi1;
	ZeroMemory(&lgi1, sizeof lgi1);
	lgi1.lgrpi1_name = GroupName;
	lgi1.lgrpi1_comment = Comment;
	NET_API_STATUS nStatus;
	int returnval = 0;

	nStatus = NetLocalGroupAdd(pszServerName, 1, (LPBYTE)&lgi1, NULL);
	switch (nStatus)
	{
	case NERR_Success:
		wprintf(L"[*]\t%s group has been successfully created on %s machine.\n", GroupName, pszServerName);
		break;
	case ERROR_ACCESS_DENIED:
		wprintf(L"[-] Access Denied\n");
		returnval = 1;
		break;
	case NERR_InvalidComputer:
		wprintf(L"[-] Computer name is invalid.\n");
		returnval = 1;
		break;
	case NERR_GroupExists || ERROR_ALIAS_EXISTS:
		wprintf(L"[-] Group already exists.\n");
		returnval = 0;
		break;
	case NERR_NotPrimary:
		wprintf(L"[-] Operation only allowed on Domain Controller.\n");
		returnval = 1;
		break;
	case NERR_SpeGroupOp:
		wprintf(L"[-]The operation is not allowed on certain special groups..\n");
		returnval = 1;
		break;
	default:
		wprintf(L"[-] Unknown Error %d\n", nStatus);
		returnval = 1;
		break;
	}

	return returnval;
}

int AddGlobalGroup(LPTSTR DomainController, LPTSTR GroupName, LPTSTR Comment)
{
	GROUP_INFO_1 gi1;
	ZeroMemory(&gi1, sizeof gi1);
	gi1.grpi1_name = GroupName;
	gi1.grpi1_comment = Comment;
	NET_API_STATUS nStatus;
	int returnval = 0;

	nStatus = NetGroupAdd(DomainController, 1, (LPBYTE)&gi1, NULL);
	switch (nStatus)
	{
	case NERR_Success:
		wprintf(L"[*]\t%s group has been successfully created on %s machine.\n", GroupName, DomainController);
		break;
	case ERROR_ACCESS_DENIED:
		wprintf(L"[-] Access Denied\n");
		returnval = 1;
		break;
	case NERR_InvalidComputer:
		wprintf(L"[-] Computer name is invalid.\n");
		returnval = 1;
		break;
	case NERR_GroupExists:
		wprintf(L"[-] Group already exists.\n");
		returnval = 0;
		break;
	case NERR_NotPrimary:
		wprintf(L"[-] Operation only allowed on Domain Controller.\n");
		returnval = 1;
		break;
	case NERR_SpeGroupOp:
		wprintf(L"[-]The operation is not allowed on certain special groups..\n");
		returnval = 1;
		break;
	default:
		wprintf(L"[-] Unknown Error %d\n", nStatus);
		returnval = 1;
		break;
	}

	return returnval;
}

int RemoveLocalGroup(LPTSTR pszServerName, LPTSTR GroupName)
{
	NET_API_STATUS nStatus;
	int returnval = 0;

	nStatus = NetLocalGroupDel(pszServerName, GroupName);
	switch (nStatus)
	{
	case NERR_Success:
		wprintf(L"[*]\t%s group has been successfully removed on %s machine.\n", GroupName, pszServerName);
		break;
	case ERROR_ACCESS_DENIED:
		wprintf(L"[-] Access Denied\n");
		returnval = 1;
		break;
	case NERR_InvalidComputer:
		wprintf(L"[-] Computer name is invalid.\n");
		returnval = 1;
		break;
	case NERR_GroupNotFound || ERROR_NO_SUCH_ALIAS:
		wprintf(L"[-] Sepecified group was not found.\n");
		returnval = 1;
		break;
	case NERR_NotPrimary:
		wprintf(L"[-] Operation only allowed on Domain Controller.\n");
		returnval = 1;
		break;
	default:
		wprintf(L"[-] Unknown Error %d\n", nStatus);
		returnval = 1;
		break;
	}

	return returnval;
}